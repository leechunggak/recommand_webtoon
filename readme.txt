cleaning.py: data.csv를 분석에 적합하게 가공합니다.
compute.py: cleaning을 거친 data로 행렬을 작성하고 예측값을 계산합니다.
recommand: compute.py로 만든 예측값으로 웹툰을 추천합니다.
learn.py: recommand로 얻은 추천 결과를 data에 반영합니다.