import csv
import numpy as np
from sklearn.metrics import mean_squared_error
import time
import cleaning
import os
import re


data_cleaning = cleaning.data
webtoon = cleaning.webtoon
email = cleaning.email
os.chdir('c:/ws/recommand_webtoon/data/matrix')
po_dir = os.popen('dir')
po_dir_read = po_dir.read()
re0 = re.compile('mat\d*.csv')
po_finda = re0.findall(po_dir_read)

mat0 = np.zeros((len(data_cleaning), len(webtoon)), dtype=np.float64)
lines_mat = []
if not po_finda:
    for line in data_cleaning:
        email_ad = line[1]
        email_index = int()
        tu_index = int()
        for word in email:
            if email_ad == word[1]:
                email_index = int(word[0])
        for word in line[4:]:
            for tu in webtoon:
                if word == tu[1]:
                    tu_index = int(tu[0])

            mat0[email_index, tu_index] = 4

else:
    with open('%s' % sorted(po_finda, reverse=True)[0], 'r', encoding='utf-8') as f:
        rdr = csv.reader(f)
        for line in rdr:
            lines_mat.append(line)

    count = 0
    for line in lines_mat:
        for j in line:
            j = int(float(j))
            if j > 0:
                mat0[count, j] = j

with open('./mat.csv', 'w', encoding='utf-8', newline='') as f:
    wr = csv.writer(f)
    for line in mat0:
        wr.writerow(list(line))
os.chdir('c:/ws/recommand_webtoon')


def compute_gd(mat, n_iter, lambda_, learning_rate, k):
    m, n = mat.shape
    errors = []

    x = np.random.rand(m, k)
    y = np.random.rand(k, n)

    for ii in range(n_iter):
        for u in range(m):
            for i in range(n):
                if mat[u, i] > 0:
                    e_ui = mat[u, i] - np.dot(x[u, :], y[:, i])
                    x[u, :] += learning_rate * (e_ui * y[:, i].T - lambda_ * x[u, :])
                    y[:, i] += learning_rate * (e_ui * x[u, :].T - lambda_ * y[:, i])
        errors.append(mean_squared_error(mat, np.dot(x, y)))

    mat_hat = np.dot(x, y)
    error_of_rate = mean_squared_error(mat, mat_hat)
    print("Error of rate: %d" % error_of_rate)

    with open('./log/log_%s.txt' % time.strftime('%x', time.localtime(time.time())).replace('/', ''),
              'a', encoding='utf-8') as f:
        f.write(time.ctime() + '\ncompute\n'
                + 'Error of rate = %d\nn_iter = %d\nlambda = %d\nlearning_rate = %d\nk = %d\n'
                % (error_of_rate, n_iter, lambda_, learning_rate, k))

    return mat_hat, errors


mat_hat0, errors0 = compute_gd(mat0, 100, 1000, 0.0001, 180)

mat_hat0 -= np.min(mat_hat0)
mat_hat0 *= float(7) / np.max(mat_hat0)
with open('./data/matrix/mat_hat.csv', 'w', encoding='utf-8', newline='') as f:
    wr = csv.writer(f)
    for line in mat_hat0:
        wr.writerow(list(line))
