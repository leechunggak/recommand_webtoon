import csv


data_raw = []
with open('./data/raw/data.csv', 'r', encoding='utf-8') as f:
    rdr = csv.reader(f)
    for line in rdr:
        data_raw.append(line)


data = data_raw

del data[0]  # 행 제목 제거

count = 0
for line in data:
    tmp_list = []
    for n in line[4:]:
        tmp_split = n.split(';')
        while '' in tmp_split:
            tmp_split.remove('')
        tmp_list.extend(tmp_split)
    del data[count][4:]
    tmp_result = list(set(tmp_list))
    data[count].extend(tmp_result)
    count += 1

with open('./data/data_cleaning.csv', 'w', encoding='utf-8', newline='') as f:
    wr = csv.writer(f)
    for line in data:
        wr.writerow(line)

email = []
with open('email.txt', 'r', encoding='utf-8') as f:
    rdr = csv.reader(f)
    for line in rdr:
        email.append(line)

webtoon = []
with open('webtoon_title.txt', 'r', encoding='utf-8') as f:
    rdr = csv.reader(f)
    for line in rdr:
        webtoon.append(line)
