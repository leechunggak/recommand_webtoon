import cleaning
import csv
import os
import re
import time


email = cleaning.email
webtoon = cleaning.webtoon


def learn(user_email):
    with open('./data/user/%s.txt' % user_email, 'r', encoding='utf-8') as f:
        lines = f.readlines()

    score = []
    count = 0
    for line in lines[1:]:
        toon = line[23:-1]
        score.append(list(input('[%s]를 재미있게 보셨나요?[y/n]\n->' % toon)))
        score[count].append(toon)
        count += 1

    os.chdir('c:/ws/recommand_webtoon/data/matrix')
    po_dir = os.popen('dir')
    po_dir_read = po_dir.read()
    re0 = re.compile('mat\d*.csv')
    po_finda = re0.findall(po_dir_read)
    lines_mat = []
    with open('./%s' % sorted(po_finda)[0], 'r', encoding='utf-8') as f:
        rdr = csv.reader(f)
        for line in rdr:
            lines_mat.append(line)

    weight = 0
    email_index = int()
    toon_index = int()
    while score:
        tmp_item = score.pop()
        for word in email:
            if word[1] == user_email:
                email_index = int(word[0])
        for word in webtoon:
            if word[1] == tmp_item[1]:
                toon_index = int(word[0])
        if tmp_item[0] == 'y':
            lines_mat[email_index][toon_index] = 5 + weight
        else:
            lines_mat[email_index][toon_index] = 3 - weight
        weight += 1

    with open('mat%s.csv' % time.strftime('%x', time.localtime(time.time())).replace('/', ''),
              'w', encoding='utf-8') as f:
        wr = csv.writer(f)
        for line in lines_mat:
            wr.writerow(list(line))
    os.chdir('c:/ws/recommand_webtoon')


insert_email = input('이메일을 입력해주십시오.\n->')
os.chdir('c:/ws/recommand_webtoon/data/user')
po = os.popen('dir')
po_read = po.read()
re0 = re.compile(insert_email)
m = re0.search(po_read)
os.chdir('c:/ws/recommand_webtoon')
if m:
    learn(insert_email)
else:
    print('이메일 주소를 확인해주십시오.')
